<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>edu.vt.middleware</groupId>
  <artifactId>duo-api-client</artifactId>
  <packaging>jar</packaging>
  <version>0.9.1</version>
  <name>duo-api-client</name>
  <description>Library for interacting with the DUO RESTful web service API.</description>
  <url>https://git.it.vt.edu/middleware/duo-api-client</url>
  <licenses>
    <license>
      <name>Apache 2</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:git@git.it.vt.edu:middleware/duo-api-client.git</connection>
    <url>scm:git:git@git.it.vt.edu:middleware/duo-api-client.git</url>
  </scm>

  <prerequisites>
    <maven>3.0.5</maven>
  </prerequisites>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <checkstyle.dir>${basedir}/src/main/checkstyle</checkstyle.dir>
  </properties>

  <dependencies>
    <!-- Compile dependencies -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <version>4.5.1</version>
    </dependency>
    <dependency>
      <groupId>com.fasterxml.jackson.core</groupId>
      <artifactId>jackson-databind</artifactId>
      <version>2.6.4</version>
    </dependency>
    <dependency>
      <groupId>joda-time</groupId>
      <artifactId>joda-time</artifactId>
      <version>2.9.1</version>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.7.13</version>
    </dependency>

    <!-- Test dependencies -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <fork>true</fork>
          <debug>true</debug>
          <showDeprecation>true</showDeprecation>
          <showWarnings>true</showWarnings>
          <compilerArgument>-Xlint:unchecked</compilerArgument>
          <source>1.7</source>
          <target>1.7</target>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>2.17</version>
        <configuration>
          <configLocation>${checkstyle.dir}/checks.xml</configLocation>
          <headerLocation>${checkstyle.dir}/header.txt</headerLocation>
          <suppressionsLocation>${checkstyle.dir}/suppressions.xml</suppressionsLocation>
          <includeTestSourceDirectory>false</includeTestSourceDirectory>
          <failsOnError>true</failsOnError>
          <outputFileFormat>plain</outputFileFormat>
          <outputFile>${project.build.directory}/checkstyle-result.txt</outputFile>
        </configuration>
        <executions>
          <execution>
            <id>checkstyle</id>
            <phase>compile</phase>
            <goals>
              <goal>checkstyle</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.10.3</version>
        <configuration>
          <links>
            <link>http://download.oracle.com/javase/7/docs/api</link>
          </links>
          <bottom><![CDATA[<i>Copyright &#169; 2016 Virginia Tech. All Rights Reserved.</i>]]></bottom>
        </configuration>
        <executions>
          <execution>
            <id>javadoc</id>
            <phase>package</phase>
            <goals>
              <goal>jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>2.6</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>

</project>
