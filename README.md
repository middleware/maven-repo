# Middleware Maven Repository

*This repository is intended for internal use only.*

## Usage
Add the following to settings.xml for global use or to pom.xml for project use:

```xml
<project>
  ...
  <repositories>
    <repository>
      <id>middleware</id>
      <name>Middleware Repository</name>
      <url>https://git.it.vt.edu/middleware/maven-repo/raw/master</url>
    </repository>
  </repositories>
  ...
  <pluginRepositories>
    <pluginRepository>
      <id>middleware</id>
      <name>Middleware Repository</name>
      <url>https://git.it.vt.edu/middleware/maven-repo/raw/master</url>
    </pluginRepository>
  </pluginRepositories>
  ...
</project>
```

## Publishing
Clone the maven-repo to your system, then you can execute a maven deploy to the local file system. Commit the changes and push them to the server to make your artifacts available.

### Maven command

```bash
mvn deploy:deploy-file \
  -DpomFile=/path/to/pom.xml \
  -Dfile=/path/to/classes.jar \
  -Djavadoc=/path/to/javadoc.jar \
  -Dsources=/path/to/sources.jar \
  -Durl=file:///path/to/maven-repo
```

